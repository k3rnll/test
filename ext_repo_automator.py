#! /usr/bin/env python
import sys
import os
import subprocess

this_script_path = sys.path[0]
this_script_dir_name = this_script_path.split('/')[-1]
ext_repo_dir = f"../{this_script_dir_name}_extrepo"
ext_repo_main_branch_name = "main"


def read_url_from_file():
    file = open(f"{this_script_path}/ext_repo_url.txt")
    url = file.readline()
    file.close()
    if url.endswith('\n'):
        url = url[:-1]
    return url


def git_clone_ext_repo():
    os.chdir(this_script_path)
    res = subprocess.run(f"git clone {read_url_from_file()} {ext_repo_dir}",
                         shell=True, capture_output=True)
    if res.returncode:
        exit(res.stderr.decode("utf-8"))
    return True


def is_ext_repo_dir():
    os.chdir(this_script_path)
    res = subprocess.run(f"ls {ext_repo_dir}",
                         shell=True, capture_output=True)
    if res.returncode != 0:
        git_clone_ext_repo()
    return True


def update_ext_repo():
    os.chdir(this_script_path)
    os.chdir(ext_repo_dir)
    subprocess.run("git fetch",
                   shell=True, capture_output=True)
    subprocess.run(f"git reset --hard origin/{ext_repo_main_branch_name}",
                   shell=True, capture_output=True)
    subprocess.run("git clean -f",
                   shell=True, capture_output=True)


def update_gitlab_repo():
    os.chdir(this_script_path)
    subprocess.run("git fetch",
                   shell=True, capture_output=True)
    subprocess.run(f"git reset --hard origin/main",
                   shell=True, capture_output=True)
    subprocess.run("git clean -d -f",
                   shell=True, capture_output=True)


def echo_last_commit_to_file():
    os.chdir(this_script_path)
    os.chdir(ext_repo_dir)
    subprocess.run(f"git log --format=oneline -5 &> {this_script_path}/ext_repo_last_commit.txt",
                   shell=True, capture_output=True)


def push_to_gitlab_repo():
    os.chdir(this_script_path)
    subprocess.run("git add . && git commit -m 'ext_repo_commit' && git push",
                   shell=True, capture_output=True)


is_ext_repo_dir()
update_ext_repo()
update_gitlab_repo()
echo_last_commit_to_file()
push_to_gitlab_repo()


